// File defines some utility functions for working with files and directories.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package file

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
)

// WalkError describes an error during a walk.
type WalkError struct {
	File string // The file
	Err  error  // The error
}

// An "error" used to terminate a successful walk.
type terminateWalk string

/////////////////////////////////////////////////////////////////////////
// terminateWalk functions
/////////////////////////////////////////////////////////////////////////

// Error returns an error message.
func (terminateWalk) Error() string {
	return "The walk has successfully terminated"
}

/////////////////////////////////////////////////////////////////////////
// WalkError functions
/////////////////////////////////////////////////////////////////////////

// Cause returns the underlying error.
func (e *WalkError) Cause() error {
	if e == nil {
		return nil
	}
	return e.Err
}

// Error returns an error message.
func (e *WalkError) Error() string {
	if e == nil {
		return ""
	}
	return fmt.Sprintf("%s: %s", e.File, e.Err)
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Walk recursively walks the file tree rooted at root. The paths to the regular files encountered (the root is included in the path) are passed down the first channel. Any directories for which we do not have permission to read will be skipped. Symbolic links will not be followed. Any errors encountered during the walk will be passed down the second channel (of type *WalkError), and the walk will continue. The third return value is a channel that can be closed to halt the walk.
func Walk(root string) (<-chan string, <-chan error, chan<- struct{}) {
	// Create the communication channels
	pathC := make(chan string)
	errC := make(chan error)
	doneC := make(chan struct{})
	// Check that the root exists
	if ok, err := Exists(root); !ok {
		if err == nil {
			err = os.ErrNotExist
		}
		go func(pathC chan<- string, errC chan<- error, doneC <-chan struct{}) {
			select {
			case errC <- &WalkError{File: root, Err: err}:
			case <-doneC:
			}
			close(pathC)
			close(errC)
		}(pathC, errC, doneC)
		return pathC, errC, doneC
	}
	// Start the walk and return
	go func(pathC chan<- string, errC chan<- error, doneC <-chan struct{}) {
		filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				// Pass the error down the errC channel
				if err == os.ErrPermission && info.IsDir() {
					return filepath.SkipDir
				}
				select {
				case errC <- &WalkError{File: path, Err: err}:
				case <-doneC:
					return terminateWalk(path)
				}
			} else if info.Mode().IsRegular() {
				// Pass the path down the pathC channel
				select {
				case pathC <- path:
				case <-doneC:
					return terminateWalk(path)
				}
			}
			return nil
		})
		close(pathC)
		close(errC)
	}(pathC, errC, doneC)
	return pathC, errC, doneC
}

// SearchRecursive recursively walks the file tree rooted at root. Any directories for which we do not have permission to read will be skipped. Symbolic links will not be followed. It will return the path (including the root) for the first file or directory matching "target". If no match is found, then returns the empty string.
func SearchRecursive(root string, target string) (pth string, err error) {
	// Create the walk function
	f := func(pth string, info os.FileInfo, err error) error {
		// Handle any incoming error
		if err != nil {
			if err == os.ErrPermission {
				if info.IsDir() {
					return filepath.SkipDir
				}
				return nil
			}
			return err
		}
		// We'll send the path back as an error to indicate a match
		if info.Name() == target {
			return terminateWalk(pth)
		}
		return nil
	}
	// Search from the root
	if err = filepath.Walk(root, f); err != nil {
		if t, ok := err.(terminateWalk); ok {
			pth = string(t)
			err = nil
		}
	}
	return
}

// SearchDir non-recursively searches the given base directory for the target file or directory. Upon success, returns the path to the target (including base).
func SearchDir(base string, target string) (pth string, err error) {
	// Open the base
	var f *os.File
	if f, err = os.Open(base); err != nil {
		return
	}
	// Recover the file info
	var fi os.FileInfo
	if fi, err = f.Stat(); err != nil {
		f.Close()
		return
	}
	// Is this a directory?
	if !fi.IsDir() {
		f.Close()
		return
	}
	// Read the directory contents
	var names []string
	if names, err = f.Readdirnames(0); err != nil {
		f.Close()
		return
	}
	// Look for a match
	for _, n := range names {
		if n == target {
			pth = path.Join(base, target)
			f.Close()
			return
		}
	}
	f.Close()
	return
}

// SearchDirs will non-recursively search the colon-separated list of directories for the first occurrence of the target file or directory. Upon success, returns the path to the target (including base). Will silently ignore any base directories that do not exist or are unreadable due to insufficient permissions.
func SearchDirs(baseDirs string, target string) (pth string, err error) {
	for _, base := range strings.Split(baseDirs, ":") {
		if pth, err = SearchDir(base, target); err != nil {
			if os.IsNotExist(err) || os.IsPermission(err) {
				err = nil
			} else {
				return
			}
		} else if pth != "" {
			return
		}
	}
	return
}

// IsRegular returns true iff the given file exists and is a regular file.
func IsRegular(pth string) (bool, error) {
	fi, err := os.Stat(pth)
	if os.IsNotExist(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return fi.Mode().IsRegular(), nil
}

// IsDir returns true iff the given file exists and is a directory.
func IsDir(pth string) (bool, error) {
	fi, err := os.Stat(pth)
	if os.IsNotExist(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return fi.IsDir(), nil
}

// Exists returns true iff the given file or directory exists.
func Exists(pth string) (bool, error) {
	_, err := os.Stat(pth)
	if os.IsNotExist(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

// RemoveFile attempts to remove the file at the given path.
func RemoveFile(pth string) error {
	f, err := os.Open(pth)
	if err != nil {
		return err
	}
	defer f.Close()
	if st, err := f.Stat(); err != nil {
		return err
	} else if st.IsDir() {
		return fmt.Errorf("%s: path is a directory", pth)
	}
	return os.Remove(pth)
}

// RemoveDir attempts to remove the directory at the given path. This will fail if the directory is non-empty.
func RemoveDir(pth string) error {
	f, err := os.Open(pth)
	if err != nil {
		return err
	}
	defer f.Close()
	if st, err := f.Stat(); err != nil {
		return err
	} else if !st.IsDir() {
		return fmt.Errorf("%s: path is not a directory", pth)
	}
	return os.Remove(pth)
}

// RemoveDirAll attempts to remove the directory at the given path, along with the contents of the directory.
func RemoveDirAll(pth string) error {
	f, err := os.Open(pth)
	if err != nil {
		return err
	}
	defer f.Close()
	if st, err := f.Stat(); err != nil {
		return err
	} else if !st.IsDir() {
		return fmt.Errorf("%s: path is not a directory", pth)
	}
	return os.RemoveAll(pth)
}
